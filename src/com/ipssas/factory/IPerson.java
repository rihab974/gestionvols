package com.ipssas.factory;

import com.ipssas.flight.Flight;
import com.ipssas.reservation.IReservation;
import com.ipssas.reservation.Passenger;
import com.ipssas.reservation.Reservation;
import org.jetbrains.annotations.NotNull;

public interface IPerson {
    void makeReservation(IReservation reservation, @NotNull Passenger passenger, Flight flight);
}
