package com.ipssas.reservation;

import com.ipssas.factory.IPerson;
import com.ipssas.factory.Person;
import com.ipssas.flight.Flight;
import org.jetbrains.annotations.NotNull;

public class Passenger extends Person implements IPerson {

    private IReservation reservation;

    public Passenger() {
    }

    protected Passenger(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public IReservation getReservation() {
        return reservation;
    }

    public void setReservation(IReservation reservation) {
        this.reservation = reservation;
    }


    @Override
    public void makeReservation(IReservation reservation, @NotNull Passenger passenger, Flight flight) {

    }
}
