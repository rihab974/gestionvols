package com.ipssas.reservation;

public abstract class ReservationDecorator implements IReservation {
    private Reservation decoratedReservation;

    public ReservationDecorator(Reservation decoratedReservation) {
        this.decoratedReservation = decoratedReservation;
    }

    @Override
    public void confirm() {
        decoratedReservation.confirm();
    }

    @Override
    public void cancel() {
        decoratedReservation.cancel();
    }
}